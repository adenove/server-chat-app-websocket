var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var PORT = process.env.PORT || 4000;
server.listen(PORT);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

app.get('/',(req,res)=>{
    res.sendFile(__dirname + '/index.html');
})

io.on('connection',(socket)=>{

    socket.on('getclient',()=>{
        io.clients((eror,client)=>{
            if(eror) throw eror;
            io.of('/').emit('getclient',client);
        })
    });

    socket.on('connectID',data => {
        io.to(data.to).emit('userConnect',data);
    });

    socket.on('getuser',() => {
        socket.emit('user',allclient);
    })

    socket.on('chatMessage',data => {
        const msg = JSON.parse(data);
        io.to(msg.to).emit('message',msg);
    });

    socket.on('calling',data =>{
        const msg = JSON.parse(data);
        io.to(msg.to).emit('calling',data);
    });

    socket.on('disconnect', function() {
        io.clients((eror,client)=>{
            if(eror) throw eror;
            io.of('/').emit('getclient',client);
        })
    });

});
